package app.service;

import app.model.Edge;
import app.model.Graph;
import app.model.Node;
import org.jgrapht.DirectedGraph;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.*;
import org.jgrapht.graph.*;

import java.util.*;

public class JGraphTService
{
    private static UndirectedGraph<Node, DefaultEdge> createUndirectedGraph() {
        UndirectedGraph<Node, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);
        Graph.nodes.values().forEach(g::addVertex);
        for(Edge edge : Graph.edges.values()) g.addEdge(edge.startNode , edge.endNode);
        return g;
    }

    private static SimpleWeightedGraph<Node, DefaultWeightedEdge> createWeightedGraph() {
        SimpleWeightedGraph<Node, DefaultWeightedEdge> g = new SimpleWeightedGraph<>(DefaultWeightedEdge.class);
        Graph.nodes.values().forEach(g::addVertex);
        for(Edge edge : Graph.edges.values()) {
            DefaultWeightedEdge weightedEdge = g.addEdge(edge.startNode, edge.endNode);
            g.setEdgeWeight(weightedEdge, 1);
        }
        return g;
    }

    private static DirectedGraph<Node, DefaultEdge> createDirectedGraph() {
        DirectedGraph<Node, DefaultEdge> g =
                new DefaultDirectedGraph<>(DefaultEdge.class);
        Graph.nodes.values().forEach(g::addVertex);
        for(Edge edge : Graph.edges.values()) g.addEdge(edge.startNode , edge.endNode);
        return g;
    }

    public static void printGraph() {
        System.out.println(createUndirectedGraph().toString());
    }

    public static List<Edge> getShortestPath(Node start, Node end) {
        UndirectedGraph<Node, DefaultEdge> graph = createUndirectedGraph();
        List<DefaultEdge> result =  DijkstraShortestPath.findPathBetween(graph, start, end);
        return getEdges(graph, result);
    }

    public static Map<Integer,Set<Node>> getColoredGroups() {
        UndirectedGraph<Node, DefaultEdge> graph = createUndirectedGraph();
        return ChromaticNumber.findGreedyColoredGroups(graph);
    }

    public static Set<Node> findCycles() {
        CycleDetector cycleDetector = new CycleDetector(createDirectedGraph());
        return cycleDetector.findCycles();
    }

    public static List<Node> findEulerianCycle() {
        UndirectedGraph<Node, DefaultEdge> graph = createUndirectedGraph();
        List<Node> result = EulerianCircuit.getEulerianCircuitVertices(graph);
        if (result != null)
            for(Node v: result) System.out.print(v.name + " ");
        return result;
    }

    public static List<Node> getHamiltonianCycle() {
        SimpleWeightedGraph<Node, DefaultWeightedEdge> graph = createWeightedGraph();
        return HamiltonianCycle.getApproximateOptimalForCompleteGraph(graph);
    }

    public static Set<Edge> getMinimumSpanningTree() {
        UndirectedGraph<Node, DefaultEdge> graph = createUndirectedGraph();
        KruskalMinimumSpanningTree kmst = new KruskalMinimumSpanningTree(graph);
        Set<DefaultEdge> mst = kmst.getMinimumSpanningTreeEdgeSet();
        return new HashSet<>(getEdges(graph, mst));
    }
    public static Set<Node> getCutpoints() {
        UndirectedGraph<Node, DefaultEdge> graph = createUndirectedGraph();
        BiconnectivityInspector bi = new BiconnectivityInspector(graph);
        return (Set<Node>) bi.getCutpoints();
    }

    public static Set<Set<Node>> getBiconnectedComponents() {
        UndirectedGraph<Node, DefaultEdge> graph = createUndirectedGraph();
        BiconnectivityInspector bi = new BiconnectivityInspector(graph);
        return bi.getBiconnectedVertexComponents();
    }

    private static List<Edge> getEdges (org.jgrapht.Graph<Node, DefaultEdge> graph, Collection<DefaultEdge> collection) {
        List<Edge> result = new ArrayList<>();
        for(DefaultEdge de : collection) {
            Node start = graph.getEdgeSource(de);
            Node end = graph.getEdgeTarget(de);
            for (Edge e : start.incidentEdges)
                if ((e.endNode == end && e.startNode == start) || (e.startNode == end && e.endNode == start)) {
                    result.add(e); break;
                }
        }
        return result;
    }
}
