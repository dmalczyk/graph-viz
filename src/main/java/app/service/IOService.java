package app.service;

import app.model.Edge;
import app.model.Graph;
import app.model.Icon;
import app.model.Node;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import javafx.scene.paint.Paint;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.util.HashSet;
import java.util.Set;

public class IOService {

    private final static ObjectMapper mapper = new ObjectMapper();

    public static void importIcons(File iconsDir) throws Exception {

        Path path = Paths.get(iconsDir.toURI());
        MessageDigest md = MessageDigest.getInstance("MD5");

        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                byte[] binary = Files.readAllBytes(file);
                try {
                    loadIcon(binary);
                } catch (Exception ex) {
                    System.out.println(ex);
                }
                return FileVisitResult.CONTINUE;
            }
        });
    }

    private static void loadIcon(byte[] binary) throws Exception {

        String hash = DigestUtils.md5Hex(binary);
        Graph.loadIcon(hash, binary);
    }

    public static void importGraph(File jsonFile) throws Exception {

        Graph.clear();

        JsonNode root = mapper.readTree(jsonFile);
        JsonNode nodesNode = root.get("nodes");
        JsonNode edgesNode = root.get("edges");
        JsonNode iconsNode = root.get("icons");

        iconsNode.forEach(iconNode -> {
            String iconBase64 = iconNode.asText();
            byte[] iconBinary = Base64.decodeBase64(iconBase64);
            try {
                loadIcon(iconBinary);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        nodesNode.forEach(nodeNode -> {
            Node node = Graph.newNode(nodeNode.get("name").asText());
            if( nodeNode.has("icon") ) {
                System.out.println(nodeNode.get("icon").asText());
                Icon icon = Graph.icons.get(nodeNode.get("icon").asText());
                node.setIcon(icon);
            }
            if( nodeNode.has("x") && nodeNode.has("y") ) {
                node.setX(nodeNode.get("x").asDouble());
                node.setY(nodeNode.get("y").asDouble());
            }
            if (nodeNode.has("style")) {
                node.setStyle(nodeNode.get("style").asText());
            }
            if(nodeNode.has("stroke")) {
                node.setStroke(Paint.valueOf(nodeNode.get("stroke").asText()));
            }
            if(nodeNode.has("fill")) {
                node.setFill(Paint.valueOf(nodeNode.get("fill").asText()));
            }
            if(nodeNode.has("height")) {
                node.setHeight(nodeNode.get("height").asDouble());
            }
            if(nodeNode.has("width")) {
                node.setWidth(nodeNode.get("width").asDouble());
            }
        });

        edgesNode.forEach(edgeNode -> {
            Node startNode = Graph.nodes.get(edgeNode.get("start").asText());
            Node endNode = Graph.nodes.get(edgeNode.get("end").asText());
            Edge edge = Graph.newEdge();
            edge.attach(startNode, endNode);
            if (edgeNode.has("style")) {
                edge.setStyle(edgeNode.get("style").asText());
            }
            if(edgeNode.has("stroke")) {
                edge.setStroke(Paint.valueOf(edgeNode.get("stroke").asText()));
                edge.setStrokeWidth(edgeNode.get("strokeWidth").doubleValue());
            }
        });

        if( !root.has("computed") || !root.get("computed").booleanValue() ) {
            Graph.applyLayout();
        }
    }

    public static void exportGraph(File output) throws IOException {

        ObjectNode root = mapper.createObjectNode();
        root.put("computed", true);
        ArrayNode nodesNode = root.putArray("nodes");
        ArrayNode edgesNode = root.putArray("edges");
        ArrayNode iconsNode = root.putArray("icons");
        Set<String> iconsUsed = new HashSet<>();

        Graph.nodes.values().stream().forEach(node -> {
            ObjectNode nodeNode = nodesNode.addObject();
            nodeNode.put("name", node.name);
            nodeNode.put("x", node.getX());
            nodeNode.put("y", node.getY());
            nodeNode.put("width", node.getWidth() * node.getScaleX());
            nodeNode.put("height", node.getHeight() * node.getScaleY());
            nodeNode.put("style", node.style);
            nodeNode.put("stroke", node.stroke.toString());
            if (node.icon != null) {
                iconsUsed.add(node.icon.hash);
                nodeNode.put("icon", node.icon.hash);
            }
            if (node.icon == null) {
                nodeNode.put("fill", node.getFill().toString());
            }
        });

        Graph.edges.values().stream().forEach(edge -> {
            if(edge.startNode != null && edge.endNode != null) {
                ObjectNode edgeNode = edgesNode.addObject();
                edgeNode.put("start", edge.startNode.name);
                edgeNode.put("end", edge.endNode.name);
                edgeNode.put("style", edge.style);
                edgeNode.put("stroke", edge.stroke.toString());
                edgeNode.put("strokeWidth", edge.getStrokeWidth());
            }
        });

        iconsUsed.stream().forEach(hash -> {
            Icon icon = Graph.icons.get(hash);
            iconsNode.add(Base64.encodeBase64String(icon.binary));
        });

        mapper.writerWithDefaultPrettyPrinter().writeValue(output, root);
    }
}
