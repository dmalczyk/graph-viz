package app.service;

import app.model.Edge;
import app.model.Graph;
import app.model.Node;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

import static app.utils.ViewUtils.inchesToPixels;
import static app.utils.ViewUtils.pixelsToInches;
import static java.lang.Double.parseDouble;

public class PlainService {

    public static String build() {

        StringBuilder sb = new StringBuilder();
        sb.append("graph generated {\n");

        Graph.nodes.values().stream().forEach( node -> {

            sb.append("\t");
            sb.append(node.name);
            sb.append(" [shape=box");

            if (node.icon != null) {

                sb.append(",width=" + pixelsToInches(node.icon.image.getWidth()));
                sb.append(",height=" + pixelsToInches(node.icon.image.getHeight()));
                sb.append(",fixedsize=true");
            }

            sb.append("];\n");
        });

        Graph.edges.values().stream().forEach( edge -> {

            sb.append("\t");
            sb.append(edge.startNode.name);
            sb.append(" -- ");
            sb.append(edge.endNode.name);
            sb.append(";\n");
        });

        sb.append("}");

        return sb.toString();
    }

    public static void load(String input) throws IOException {

        String line;
        BufferedReader inReader = new BufferedReader(new StringReader(input));
        DoubleProperty minX = new SimpleDoubleProperty(0), minY = new SimpleDoubleProperty(0);

        while((line = inReader.readLine()) != null) {

            String[] tokens = line.split(" ");

            switch (tokens[0]) {

                case "node":

                    Node node = Graph.nodes.get(tokens[1]);

                    double nwidth = inchesToPixels(parseDouble(tokens[4]));
                    double nheight = inchesToPixels(parseDouble(tokens[5]));

                    node.setWidth(nwidth);
                    node.setHeight(nheight);

                    double x = inchesToPixels((parseDouble(tokens[2])))-nwidth/2;
                    double y = inchesToPixels((parseDouble(tokens[3])))-nheight/2;

                    if( x < minX.get() ) minX.setValue(x);
                    if( y < minY.get() ) minY.setValue(y);

                    node.setX(x);
                    node.setY(y);

                    break;

                case "edge":

                    Node start = Graph.nodes.get(tokens[2]);
                    Node end = Graph.nodes.get(tokens[1]);

                    Edge edge = start.incidentEdges.stream().filter(end.incidentEdges::contains).findAny().get();
                    edge.attach(start, end);

                    break;
            }

        }

        Graph.nodes.values().stream().forEach( node -> {
            node.setX(node.getX() + Math.abs(minX.get()));
            node.setY(node.getY() + Math.abs(minY.get()));
        });
    }
}
