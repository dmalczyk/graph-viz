package app.service;

import java.io.*;

public class GraphvizService {

    public static String layout = "fdp";
    public static String GRAPHVIZ_HOME = "C:\\Program Files (x86)\\Graphviz2.38\\bin\\";

    public static String run(String input) {

        Process process;
        StringBuilder output = new StringBuilder();

        try {

            process = new ProcessBuilder()
                .command( GRAPHVIZ_HOME + layout + ".exe",
                    "-Gsplines=false",
                    "-Goverlap=scale",
                    "-Tplain",
                    "-y"
                ).start();

            BufferedWriter processInput = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
            BufferedReader dotStream = new BufferedReader(new StringReader(input));

            String dotLine;
            while((dotLine = dotStream.readLine()) != null)
                processInput.write(dotLine);

            processInput.flush();
            processInput.close();

            BufferedReader processOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String outputLine;
            while((outputLine = processOutput.readLine()) != null) {
                output.append(outputLine);
                output.append("\n");
            }

            processOutput.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return output.toString();
    }
}
