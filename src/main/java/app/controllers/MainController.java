package app.controllers;

import app.Application;
import app.actions.Selection;
import app.model.Edge;
import app.model.Graph;
import app.model.Icon;
import app.model.Node;
import app.service.GraphvizService;
import app.service.IOService;
import app.service.JGraphTService;
import app.utils.ColorUtils;
import app.actions.changes.ChangeManager;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.MapChangeListener;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

public class MainController implements Initializable {

    @FXML public AnchorPane root;
    @FXML public AnchorPane graphPane;
    @FXML public Button reloadButton;
    @FXML public ScrollPane scrollPane;
    @FXML public ScrollPane iconsScroll;
    @FXML public FlowPane iconsPane;
    @FXML public ToggleButton toggle1;
    @FXML public ToggleButton toggle2;
    @FXML public ToggleButton toggle3;
    @FXML public ToggleButton toggle4;
    @FXML public ToggleButton toggle5;
    @FXML public ImageView bicomp;
    @FXML public ImageView coloring;
    @FXML public ImageView mst;
    @FXML public ImageView euler;
    @FXML public ImageView clear;
    @FXML public CheckMenuItem labelsItem;

    public static MainController inst;
    @FXML public MenuItem undoItem;
    @FXML public MenuItem redoItem;

    public ChangeManager changeManager = new ChangeManager();
    @FXML public StackPane zoomPane;
    final double ZOOM_FACTOR = 1.1;
    private Group graphGroup;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        inst = this;
        scrollPane.contentProperty().setValue(Graph.view);
        graphGroup = Graph.graphGroup;
        StackPane.setMargin(graphGroup, new Insets(8,8,8,8));
        createZoomPane(Graph.view);
        scrollPane.setStyle("-fx-background: #ffffff;");
        scrollPane.setOnMousePressed(e -> Selection.clear());

        DoubleBinding iconsScrollWidthBinding = new DoubleBinding() {
            {bind(iconsScroll.viewportBoundsProperty());}
            @Override
            protected double computeValue() {
                return iconsScroll.getViewportBounds().getWidth();
            }
        };

        iconsPane.prefWrapLengthProperty().bind(iconsScrollWidthBinding);
        iconsPane.prefWidthProperty().bind(iconsScrollWidthBinding);


        Graph.icons.addListener((MapChangeListener<String, Icon>) change -> {
            if (change.wasAdded()) {
                iconsPane.getChildren().add(change.getValueAdded());
            }
        });

        root.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.DELETE) {
                Selection.delete();
            }
        });

        coloring.setImage(new Image(String.valueOf(getClass().getClassLoader().getResource("icons/Coloring.gif"))));
        mst.setImage(new Image(String.valueOf(getClass().getClassLoader().getResource("icons/MST.gif"))));
        euler.setImage(new Image(String.valueOf(getClass().getClassLoader().getResource("icons/Euler.gif"))));
        bicomp.setImage(new Image(String.valueOf(getClass().getClassLoader().getResource("icons/BiComp.gif"))));
        clear.setImage(new Image(String.valueOf(getClass().getClassLoader().getResource("icons/Rubber.png"))));

        toggle1.setTooltip(new Tooltip("Get coloring of the graph"));
        toggle2.setTooltip(new Tooltip("Get minimum spanning tree"));
        toggle3.setTooltip(new Tooltip("Check if graph has Eulerian cycle"));
        toggle4.setTooltip(new Tooltip("Get biconnected components and cutpoints"));
        toggle5.setTooltip(new Tooltip("Reset all additional strokes"));

        ToggleGroup group = new ToggleGroup();
        toggle1.setToggleGroup(group);
        toggle2.setToggleGroup(group);
        toggle3.setToggleGroup(group);
        toggle4.setToggleGroup(group);
        group.selectedToggleProperty().addListener((observable, oldValue, newValue) -> Graph.resetStrokes());
        labelsItem.setSelected(true);

        undoItem.disableProperty().bind(Bindings.not(changeManager.undoAvailableProperty()));
        redoItem.disableProperty().bind(Bindings.not(changeManager.redoAvailableProperty()));
        undoItem.setOnAction((e1) -> changeManager.undo());
        redoItem.setOnAction((e1) -> changeManager.redo());
        undoItem.setAccelerator(new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN));
        redoItem.setAccelerator(new KeyCodeCombination(KeyCode.Y, KeyCombination.CONTROL_DOWN));
    }

    @FXML
    public void setLayoutNeato(ActionEvent actionEvent) throws Exception {
        GraphvizService.layout = "neato";
        Graph.applyLayout();
    }

    @FXML
    public void setLayoutFdp(ActionEvent actionEvent) throws Exception {
        GraphvizService.layout = "fdp";
        Graph.applyLayout();
    }

    @FXML
    public void setLayoutSfdp(ActionEvent actionEvent) throws Exception {
        GraphvizService.layout = "sfdp";
        Graph.applyLayout();
    }

    @FXML
    public void setLayoutCirco(ActionEvent actionEvent) throws Exception {
        GraphvizService.layout = "circo";
        Graph.applyLayout();
    }

    @FXML
    public void setLayoutPw(ActionEvent actionEvent) throws Exception {
        GraphvizService.layout = "patchwork";
        Graph.applyLayout();
    }

    @FXML
    public void setLayoutTwopi(ActionEvent actionEvent) throws Exception {
        GraphvizService.layout = "twopi";
        Graph.applyLayout();
    }

    @FXML
    public void clearAllAction(ActionEvent actionEvent) throws Exception {
        Graph.clear();
        changeManager.refresh();
    }

    @FXML
    public void loadToPalette(ActionEvent actionEvent) throws Exception {

        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Choose folder with icons");
        chooser.setInitialDirectory(new File(System.getProperty("user.dir")));

        File folder = chooser.showDialog(Application.stage);

        if(folder == null) {
            return;
        }

        IOService.importIcons(folder);
    }

    @FXML
    public void loadGraphAction(ActionEvent actionEvent) throws Exception {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select input file");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON", "*.json"));

        File inputFile = fileChooser.showOpenDialog(Application.stage);
        if(inputFile == null) {
            return;
        }

        IOService.importGraph(inputFile);
    }

    @FXML
    public void saveGraphAction(ActionEvent actionEvent) throws Exception {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select output file");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON", "*.json"));

        File output = fileChooser.showSaveDialog(Application.stage);
        if(output == null) {
            return;
        }

        if(!output.getName().contains(".")) {
            output = new File(output.getAbsolutePath() + ".json");
        }

        IOService.exportGraph(output);
    }

    @FXML
    public void getColoredGroupsAction(ActionEvent actionEvent) {
        if (toggle1.isSelected()) {
            Map<Integer, Set<Node>> coloredGroups = JGraphTService.getColoredGroups();
            int chromaticNumber = coloredGroups.keySet().size();
            List<Paint> colors = ColorUtils.getRandomColors(chromaticNumber);
            if (coloredGroups.keySet().isEmpty()) return;
            for (Map.Entry<Integer, Set<Node>> group : coloredGroups.entrySet()) {
                for (Node node : group.getValue())
                    node.color(colors.get(group.getKey()));
            }
        }
    }

    @FXML
    public void getMinimumSpanningTreeAction(ActionEvent actionEvent) {
        if (toggle2.isSelected()) {
            Set<Edge> minimumSpanningTree = JGraphTService.getMinimumSpanningTree();
            Paint p = ColorUtils.getRandomColor();
            if (minimumSpanningTree == null) return;
            for(Edge edge : minimumSpanningTree) edge.color(p);
        }
    }

    @FXML
    public void findEulerianCycleAction(ActionEvent actionEvent) {
        if(toggle3.isSelected()) {
            List<Node> cycles = JGraphTService.findEulerianCycle();
            Paint p = ColorUtils.getRandomColor();
            if (cycles == null) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                alert.setContentText("There is no Eulerian cycle in this graph.");
                alert.showAndWait();
                return;
            }
            for(Node node : cycles) {
                node.color(p);
                node.incidentEdges
                        .stream()
                        .filter(edge -> cycles.contains(edge.startNode) && cycles.contains(edge.startNode))
                        .forEach(edge -> edge.color(p));
            }
        }
    }

    @FXML
    public void getBiconnectedComponentsAction(ActionEvent actionEvent) {
        if(toggle4.isSelected()) {
            Set<Node> cutpoints = JGraphTService.getCutpoints();
            Set<Set<Node>> biconnectedComponents = JGraphTService.getBiconnectedComponents();
            if (cutpoints.isEmpty()) return;
            int i = 1;
            for(Set<Node> component : biconnectedComponents) {
                for(Node node : component) node.color(ColorUtils.getColorList().get(i));
                i++;
            }
            for (Node cutpoint : cutpoints) cutpoint.setStyle("-fx-stroke-width: 5; -fx-stroke-dash-array: 12 12 12 12;");
        }
    }

    @FXML public void clearStrokesAction(ActionEvent actionEvent) {
        Graph.resetStrokes();
    }

    @FXML
    public void zoomIn() {
        Graph.view.setScaleX(Graph.view.getScaleX() * ZOOM_FACTOR);
        Graph.view.setScaleY(Graph.view.getScaleY() * ZOOM_FACTOR);
    }

    @FXML
    public void zoomOut() {
        Graph.view.setScaleX(Graph.view.getScaleX() / ZOOM_FACTOR);
        Graph.view.setScaleY(Graph.view.getScaleY() / ZOOM_FACTOR);
    }
    @FXML
    public void resetZoom() {
        Graph.view.setScaleX(1);
        Graph.view.setScaleY(1);
    }

    @FXML public void saveAsImgAction() {
        WritableImage img = graphGroup.snapshot(new SnapshotParameters(), null);

        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
        fileChooser.getExtensionFilters().add(extFilter);

        File file = fileChooser.showSaveDialog(Application.stage);

        if(file != null) {
            try {
                ImageIO.write(SwingFXUtils.fromFXImage(img, null), "png", file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void createZoomPane(Pane group) {
        zoomPane.getChildren().add(group);
        Group scrollableContent = new Group(zoomPane);
        scrollPane.contentProperty().setValue(scrollableContent);
        scrollPane.setPannable(true);
        scrollPane.setHbarPolicy(javafx.scene.control.ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setVbarPolicy(javafx.scene.control.ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.viewportBoundsProperty().addListener((observable, oldValue, newValue) -> {
            zoomPane.setMinSize(newValue.getWidth(), newValue.getHeight());
        });

        scrollPane.setPrefViewportWidth(250);
        scrollPane.setPrefViewportHeight(250);

        zoomPane.setOnScroll(event -> {
            event.consume();

            if (event.getDeltaY() == 0) return;

            double factor = (event.getDeltaY() > 0) ? ZOOM_FACTOR : 1 / ZOOM_FACTOR;

            Point2D offset = getScrollbarOffset(scrollableContent, scrollPane);

            group.setScaleX(group.getScaleX() * factor);
            group.setScaleY(group.getScaleY() * factor);

            adjustScrollPane(scrollableContent, scrollPane, factor, offset);
        });

        final ObjectProperty<Point2D> lastCoordinates = new SimpleObjectProperty<Point2D>();
        scrollableContent.setOnMousePressed(event -> lastCoordinates.set(new Point2D(event.getX(), event.getY())));

        scrollableContent.setOnMouseDragged(event -> {
            if(event != null && lastCoordinates.get() != null){
                double dX = event.getX() - lastCoordinates.get().getX();
                double extraWidth = scrollableContent.getLayoutBounds().getWidth() - scrollPane.getViewportBounds().getWidth();
                double dH = dX * (scrollPane.getHmax() - scrollPane.getHmin()) / extraWidth;
                double desiredHvalue = scrollPane.getHvalue() - dH;
                scrollPane.setHvalue(Math.max(0, Math.min(scrollPane.getHmax(), desiredHvalue)));

                double dY = event.getY() - lastCoordinates.get().getY();
                double extraHeight = scrollableContent.getLayoutBounds().getHeight() - scrollPane.getViewportBounds().getHeight();
                double dV = dY * (scrollPane.getHmax() - scrollPane.getHmin()) / extraHeight;
                double desiredVvalue = scrollPane.getVvalue() - dV;
                scrollPane.setVvalue(Math.max(0, Math.min(scrollPane.getVmax(), desiredVvalue)));
            }
        });

        //return scrollPane;
    }

    private Point2D getScrollbarOffset(javafx.scene.Node scrollableContent, ScrollPane scrollPane) {
        double extraWidth = scrollableContent.getLayoutBounds().getWidth() - scrollPane.getViewportBounds().getWidth();
        double hScrollProportion = (scrollPane.getHvalue() - scrollPane.getHmin()) / (scrollPane.getHmax() - scrollPane.getHmin());
        double scrollXOffset = hScrollProportion * Math.max(0, extraWidth);
        double extraHeight = scrollableContent.getLayoutBounds().getHeight() - scrollPane.getViewportBounds().getHeight();
        double vScrollProportion = (scrollPane.getVvalue() - scrollPane.getVmin()) / (scrollPane.getVmax() - scrollPane.getVmin());
        double scrollYOffset = vScrollProportion * Math.max(0, extraHeight);
        return new Point2D(scrollXOffset, scrollYOffset);
    }

    private void adjustScrollPane(javafx.scene.Node scrollableContent, ScrollPane scrollPane, double factor, Point2D offset) {
        double xOffset = offset.getX();
        double yOffset = offset.getY();
        double extraWidth = scrollableContent.getLayoutBounds().getWidth() - scrollPane.getViewportBounds().getWidth();

        if (extraWidth > 0) {
            double halfWidth = scrollPane.getViewportBounds().getWidth() / 2 ;
            double newScrollXOffset = (factor - 1) *  halfWidth + factor * xOffset;
            scrollPane.setHvalue(scrollPane.getHmin() + newScrollXOffset * (scrollPane.getHmax() - scrollPane.getHmin()) / extraWidth);
        } else scrollPane.setHvalue(scrollPane.getHmin());

        double extraHeight = scrollableContent.getLayoutBounds().getHeight() - scrollPane.getViewportBounds().getHeight();

        if (extraHeight > 0) {
            double halfHeight = scrollPane.getViewportBounds().getHeight() / 2 ;
            double newScrollYOffset = (factor - 1) * halfHeight + factor * yOffset;
            scrollPane.setVvalue(scrollPane.getVmin() + newScrollYOffset * (scrollPane.getVmax() - scrollPane.getVmin()) / extraHeight);
        } else scrollPane.setHvalue(scrollPane.getHmin());
    }
    
}
