package app.model;

import app.Context;
import app.actions.Selection;
import app.controllers.MainController;
import app.service.GraphvizService;
import app.service.PlainService;
import javafx.beans.binding.DoubleBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.geometry.Bounds;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Paint;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public final class Graph extends StackPane {

    public final static Map<String, Node> nodes = new HashMap<>();
    public final static Map<Integer, Edge> edges = new HashMap<>();
    public final static ObservableMap<String, Icon> icons = FXCollections.observableHashMap();

    public final static Group graphGroup = new Group();

    public final static StackPane view = new Graph();

    public static Edge newEdge() {

        Edge edge = new Edge();
        edges.put(edge.id, edge);
        graphGroup.getChildren().addAll(edge, edge.label);
        return edge;
    }

    public static void removeEdge(Edge edge) {

        edge.detach();
        edges.remove(edge.id);
        graphGroup.getChildren().removeAll(edge, edge.label);
    }

    public static Node newNode(String name) {

        Node node = new Node(name);
        nodes.put(node.name, node);
        graphGroup.getChildren().addAll(node.background, node, node.label);
        return node;
    }

    public static void removeNode(Node node) {

        node.deleteAllIncidentEdges();
        nodes.remove(node.name);
        graphGroup.getChildren().removeAll(node.background, node, node.label);
    }

    public static Icon loadIcon(String hash, byte[] imageBinary) {

        return icons.computeIfAbsent(hash, k -> new Icon(hash, imageBinary));
    }

    public static void unloadIcon(Icon icon) {

        icons.remove(icon.hash);
    }

    public static void clear() {

        nodes.clear();
        edges.clear();
        graphGroup.getChildren().clear();
        Selection.nodes.clear();
    }

    public static void applyLayout() throws IOException {
        String plainIn = PlainService.build();
        System.out.println(plainIn);
        String plainOut = GraphvizService.run(plainIn);
        System.out.println(plainOut);
        PlainService.load(plainOut);
    }

    public static void clearIcons() {

        icons.clear();
    }

    private Graph() {

        parentProperty().addListener((obs, old, newv) -> {

            ScrollPane parent = MainController.inst.scrollPane;
            Bounds bounds = graphGroup.getBoundsInParent();

            DoubleBinding widthBinding = new DoubleBinding() {
                {
                    bind(parent.viewportBoundsProperty(), graphGroup.boundsInParentProperty());
                }

                @Override
                protected double computeValue() {
                    if (bounds.getWidth() > parent.getViewportBounds().getWidth()) {
                        return bounds.getWidth();
                    } else {
                        return parent.getViewportBounds().getWidth();
                    }
                }
            };

            DoubleBinding heightBinding = new DoubleBinding() {
                {
                    bind(parent.viewportBoundsProperty(), graphGroup.boundsInParentProperty());
                }

                @Override
                protected double computeValue() {
                    if (bounds.getHeight() > parent.getViewportBounds().getHeight()) {
                        return bounds.getHeight();
                    } else {
                        return parent.getViewportBounds().getHeight();
                    }
                }
            };

            prefWidthProperty().bind(widthBinding);
            prefHeightProperty().bind(heightBinding);
        });

        setOnMouseReleased(e -> {

            if (Context.newNode != null &&
                e.getX() > 0 &&
                e.getX() <= getPrefWidth() &&
                e.getY() >= 0 &&
                e.getY() <= getPrefHeight()) {

                TextInputDialog name = new TextInputDialog("");
                name.setTitle("New node");
                name.setContentText("Node name:");
                name.setHeaderText("");

                final Button okButton = (Button) name.getDialogPane().lookupButton(ButtonType.OK);
                okButton.addEventFilter(ActionEvent.ACTION, ae -> {
                    if (Graph.nodes.containsKey(name.getEditor().getText())) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("New node");
                        alert.setHeaderText("Error");
                        alert.setContentText("Ooops, node with given name already exists. Try different name.");

                        alert.showAndWait();
                        ae.consume(); //not valid
                    }
                });
                Optional<String> result = name.showAndWait();

                if (result.isPresent()) {
                    Bounds bounds = graphGroup.getBoundsInLocal();
                    Node node = newNode(result.get());
                    node.setIcon(Context.newNode);
                    node.setX(e.getX() + bounds.getMinX());
                    node.setY(e.getY() + bounds.getMinY());
                }
            }
        });

        setOnMousePressed(e -> {
            Selection.clear();
            if( Context.nodeMenu != null ){
                Context.nodeMenu.hide();
                Context.nodeMenu = null;
            }
        });

        setBackground(new Background(new BackgroundFill(Paint.valueOf("white"), null, null)));
        setAlignment(Pos.TOP_LEFT);
        getChildren().add(graphGroup);
    }

    public static void resetStrokes() {
        Graph.nodes.values().forEach(Node::resetStroke);
        Graph.edges.values().forEach(Edge::resetStroke);
    }
}
