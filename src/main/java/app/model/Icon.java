package app.model;

import app.Context;
import app.controllers.MainController;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

import java.io.ByteArrayInputStream;

public class Icon extends ImageView {

    public static final int MAX_WIDTH = 100;
    public static final int MAX_HEIGHT = 75;

    public final String hash;
    public final byte[] binary;
    public final Image image;

    public Icon(String hash, byte[] imageBinary) {

        this.hash = hash;
        this.binary = imageBinary;

        ByteArrayInputStream stream = new ByteArrayInputStream(imageBinary);
        image = new Image(stream);
        if( image.isError() ) {
            throw new RuntimeException(image.getException());
        }

        double width = image.getWidth() > MAX_WIDTH ? MAX_WIDTH : image.getWidth();
        double height = image.getHeight() > MAX_HEIGHT ? MAX_HEIGHT : image.getHeight();

        setFitWidth(width);
        setFitHeight(height);
        setImage(image);
        setPreserveRatio(true);

        listenForDragging();
    }

    private void listenForDragging() {

        AnchorPane root = MainController.inst.root;

        setOnMousePressed((e1) -> {

            Rectangle moveShape = new Rectangle();
            moveShape.setX(e1.getSceneX());
            moveShape.setY(e1.getSceneY());
            moveShape.setWidth(image.getWidth());
            moveShape.setHeight(image.getHeight());

            moveShape.setFill(Color.rgb(0, 0, 0, 0.0));
            moveShape.setStroke(Paint.valueOf("green"));
            moveShape.getStrokeDashArray().addAll(2.0, 2.0);

            root.getChildren().add(moveShape);

            Context.newNode = this;

            setOnMouseDragged(e3 -> {
                moveShape.setX(e3.getSceneX());
                moveShape.setY(e3.getSceneY());
            });

            setOnMouseReleased(e2 -> {
                Graph.view.fireEvent(e2);
                root.getChildren().remove(moveShape);
                Context.newNode = null;
            });
        });
    }
}
