package app.model;

import app.Context;
import app.actions.Selection;
import app.controllers.MainController;
import app.actions.changes.impl.EdgeColorChange;
import app.actions.changes.impl.EdgeStyleChange;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeLineCap;

import java.util.concurrent.atomic.AtomicBoolean;

public class Edge extends Line {

    public final int id;
    public final Label label;
    public Node startNode;
    public Node endNode;
    public Paint stroke = Color.GRAY;
    public String style = "-fx-stroke-width: 2;";

    private static int nextId = 0;

    public Edge() {
        id = nextId++;
        setVisible(false);
        setSmooth(true);
        setStroke(stroke);
        setStyle(style);
        listenForClicking();
        //Tooltip.install(this, new Tooltip(Integer.toString(id)));
        this.label = new Label(Integer.toString(id));
        label.setStyle("-fx-font-weight: bold;");
    }

    public void attach(Node newStartNode, Node newEndNode) {

        if ( newStartNode == startNode && newEndNode == endNode) {
            return;
        }

        toBack();
        setVisible(true);
        startNode = newStartNode;
        endNode = newEndNode;
        startNode.incidentEdges.add(this);
        endNode.incidentEdges.add(this);

        startXProperty().bind(startNode.xProperty().add(startNode.widthProperty().divide(2)));
        startYProperty().bind(startNode.yProperty().add(startNode.heightProperty().divide(2)));

        endXProperty().bind(endNode.xProperty().add(endNode.widthProperty().divide(2)));
        endYProperty().bind(endNode.yProperty().add(endNode.heightProperty().divide(2)));

        label.layoutXProperty().bind((endXProperty().add(startXProperty())).multiply(0.5));
        label.layoutYProperty().bind((endYProperty().add(startYProperty())).multiply(0.5));
        label.visibleProperty().bind(MainController.inst.labelsItem.selectedProperty());
    }

    public void detach() {

        if( startNode != null ) {
            startNode.incidentEdges.remove(this);
        }
        if( endNode != null ) {
            endNode.incidentEdges.remove(this);
        }
        startNode = null;
        endNode = null;
    }

    public void select() {
        setStyle("-fx-effect: dropshadow(two-pass-box, #323bff, 3, 1, 0, 0);");
    }

    public void deselect() {
        resetStroke();
    }

    public void resetStroke() {
        setStroke(stroke);
        setStyle(style);
    }

    public ContextMenu getContextMenu() {

        ContextMenu cm = new ContextMenu();

        MenuItem deleteItem = new MenuItem(null, new Label("Delete edge"));
        deleteItem.setOnAction((e1) -> {
            this.detach();
            Graph.removeEdge(this);
        });

        Menu styleMenu = new Menu(null, new Label("Line style"));

        MenuItem plain = new CheckMenuItem("Plain");
        plain.setOnAction((e1) -> {
            EdgeStyleChange ch = new EdgeStyleChange(this, style, "-fx-stroke-width: 2;");
            MainController.inst.changeManager.execute(ch);
            this.deselect();
        });

        MenuItem dotted = new MenuItem("Dotted");
        dotted.setOnAction((e1) -> {
            EdgeStyleChange ch = new EdgeStyleChange(this, style, "-fx-stroke-width: 2; -fx-stroke-dash-array: 1 15;");
            MainController.inst.changeManager.execute(ch);
            this.deselect();
        });

        MenuItem dashed = new MenuItem("Dashed");
        dashed.setOnAction((e1) -> {
            EdgeStyleChange ch = new EdgeStyleChange(this, style, "-fx-stroke-width: 2; -fx-stroke-dash-array: 12 12 12 12;");
            MainController.inst.changeManager.execute(ch);
            this.deselect();
        });

        styleMenu.getItems().addAll(plain, dashed, dotted);

        final ColorPicker colorPicker = new ColorPicker();
        colorPicker.setStyle("-fx-background-color: white;");
        colorPicker.setValue((Color) getStroke());

        MenuItem colorItem = new MenuItem(null,colorPicker);
        colorItem.setOnAction((e1) ->  {
            EdgeColorChange ch = new EdgeColorChange(this, stroke, colorPicker.getValue());
            MainController.inst.changeManager.execute(ch);
            this.deselect();
        });

        Slider edgeSlider = new Slider(0.5, 5, getStrokeWidth());
        MenuItem resizeItem = new MenuItem("Line width", edgeSlider);
        this.strokeWidthProperty().bind(edgeSlider.valueProperty());
        SeparatorMenuItem separator = new SeparatorMenuItem();

        cm.getItems().addAll(deleteItem, separator, styleMenu, colorItem, resizeItem);
        return cm;
    }

    private void listenForClicking() {
        setOnMousePressed((e1) -> {
            if(Context.edgeMenu != null) {Context.edgeMenu.hide(); Context.edgeMenu = null;}
            if(Context.nodeMenu != null) {Context.nodeMenu.hide(); Context.nodeMenu = null;}
            AtomicBoolean wasSelected = new AtomicBoolean(false);
            AnchorPane root = MainController.inst.root;
            if ( !Selection.edges.contains(this) ) {
                Selection.selectEdge(this);
                wasSelected.set(true);
            }

            setOnMouseReleased(e2 -> {
                if (e1.isSecondaryButtonDown()) {
                   Context.edgeMenu = getContextMenu();
                   Context.edgeMenu.show(root, e2.getScreenX(), e2.getScreenY());
                } else if (!wasSelected.get()) {
                    Selection.deselectEdge(this);
                }
            });

            e1.consume();
        });

    }

    public void color(Paint paint) {
        setStroke(paint);
        setStyle("-fx-stroke-width: 4;");
        setStrokeLineCap(StrokeLineCap.ROUND);
    }


}
