package app.model;

import app.Context;
import app.actions.Selection;
import app.controllers.MainController;
import app.service.JGraphTService;
import app.actions.changes.impl.IconChange;
import app.actions.changes.impl.NodeColorChange;
import app.actions.changes.impl.NodeStyleChange;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Point2D;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class Node extends Rectangle {

    public final String name;
    public final Label label;
    public final Set<Edge> incidentEdges = new HashSet<>();
    public Icon icon;
    public Rectangle background;
    public Paint stroke = Color.GRAY;
    public String style = "";
    public Node(String name) {

        this.name = name;
        setStroke(stroke);
        background = new Rectangle();
        background.widthProperty().bind(widthProperty());
        background.heightProperty().bind(heightProperty());
        background.xProperty().bind(xProperty());
        background.yProperty().bind(yProperty());
        background.setFill(Color.WHITE);
        this.label = new Label(name);
        label.layoutXProperty().bind(background.xProperty());
        label.layoutYProperty().bind(background.yProperty().add(background.heightProperty().add(5)));
        label.visibleProperty().bind(MainController.inst.labelsItem.selectedProperty());
        label.setStyle("-fx-font-weight: bold;");
        resetStroke();
        listenForDragging();
    }

    public void setIcon(Icon newIcon) {
        icon = newIcon;
        setHeight(newIcon.image.getHeight());
        setWidth(newIcon.image.getWidth());
        ImagePattern imagePattern = new ImagePattern(icon.image);
        setFill(imagePattern);
    }

    public void deleteAllIncidentEdges() {
        Set<Edge> copy = incidentEdges.stream().collect(Collectors.toSet());
        copy.stream().forEach(Graph::removeEdge);
    }

    public ContextMenu getContextMenu() {

        ContextMenu cm = new ContextMenu();

        MenuItem newEdgeItem = new MenuItem(null, new Label("New edge"));
        newEdgeItem.setOnAction(e -> {
            AnchorPane root = MainController.inst.root;
            Context.newEdge = new Line();
            Context.newEdgeStart = this;
            Graph.view.setOnMouseMoved( e1 -> {
                Context.newEdge.setEndX(e1.getSceneX()-4);
                Context.newEdge.setEndY(e1.getSceneY()-4);
            });

            Point2D global = localToScene(getX() + getWidth()/2, getY() + getHeight() / 2);
            Context.newEdge.setStartX(global.getX());
            Context.newEdge.setStartY(global.getY());
            root.getChildren().add(Context.newEdge);
        });

        MenuItem shortestPathItem = new MenuItem(null, new Label("Get shortest path"));
        shortestPathItem.setOnAction(e -> Context.shortestPathStart = this);

        MenuItem delItem = new MenuItem(null, new Label("Delete"));
        delItem.setOnAction(e -> Graph.removeNode(this));

        MenuItem delEdgesItem = new MenuItem(null, new Label("Delete all edges"));
        delEdgesItem.setOnAction(e -> deleteAllIncidentEdges());

        SeparatorMenuItem separator1 = new SeparatorMenuItem();

        final ColorPicker colorPicker = new ColorPicker();
        colorPicker.setStyle("-fx-background-color: white;");
        colorPicker.setValue((Color) getStroke());
        MenuItem strokeItem = new MenuItem("Stroke",colorPicker);
        strokeItem.setOnAction((e1) ->  {
            NodeColorChange ch = new NodeColorChange(this, stroke, colorPicker.getValue());
            MainController.inst.changeManager.execute(ch);
            this.deselect();
        });

        Menu styleMenu = new Menu(null, new Label("Border style"));

        MenuItem transparent = new MenuItem("Transparent");
        transparent.setOnAction((e1) -> {
            NodeColorChange ch = new NodeColorChange(this, stroke, Color.TRANSPARENT);
            MainController.inst.changeManager.execute(ch);
            this.deselect();
        });

        MenuItem solid = new MenuItem("Solid");
        solid.setOnAction((e1) -> {
            NodeStyleChange ch = new NodeStyleChange(this, style, "-fx-stroke-width: 4;");
            MainController.inst.changeManager.execute(ch);
            this.deselect();
        });

        MenuItem dashed = new MenuItem("Dashed");
        dashed.setOnAction((e1) -> {
            NodeStyleChange ch = new NodeStyleChange(this, style, "-fx-stroke-width: 2; -fx-stroke-dash-array: 12 12 12 12;");
            MainController.inst.changeManager.execute(ch);
            this.deselect();
        });
        styleMenu.getItems().addAll(transparent, solid, dashed);
        if (getStroke()== Color.TRANSPARENT)  {
            solid.setDisable(true);
            dashed.setDisable(true);
        }

        SeparatorMenuItem separator2 = new SeparatorMenuItem();
        Menu iconMenu = new Menu(null, new Label("Select icon"));

        for(Icon icon: Graph.icons.values()){
            ImageView imageView = new ImageView(icon.image);
            imageView.setFitWidth(48); imageView.setFitHeight(48);
            MenuItem menuItem = new MenuItem(null, imageView);
            menuItem.setOnAction((e1) -> {
                IconChange ch = new IconChange(this, this.icon, icon);
                MainController.inst.changeManager.execute(ch);
                this.deselect();
            });
            iconMenu.getItems().add(menuItem);
        }

        Slider nodeSlider = new Slider(0.5, 1.5, this.scaleXProperty().doubleValue());
        MenuItem resizeItem = new MenuItem("Resize node", nodeSlider);
        this.scaleXProperty().bindBidirectional(nodeSlider.valueProperty());
        this.scaleYProperty().bindBidirectional(nodeSlider.valueProperty());
        this.background.scaleXProperty().bindBidirectional(nodeSlider.valueProperty());
        this.background.scaleYProperty().bindBidirectional(nodeSlider.valueProperty());

        cm.getItems().addAll(newEdgeItem, shortestPathItem, separator1, delItem, delEdgesItem, separator2, styleMenu, iconMenu, strokeItem, resizeItem);

        if (getFill() == Color.TRANSPARENT){
            transparent.setDisable(true);
            final ColorPicker colorPicker1 = new ColorPicker();
            colorPicker1.setStyle("-fx-background-color: white;");
            colorPicker1.setValue((Color) getFill());
            MenuItem fillItem = new MenuItem("Fill",colorPicker1);
            fillItem.setOnAction((e1) ->  {
                setFill(colorPicker1.getValue());
                this.deselect();
            });
            cm.getItems().add(fillItem);
        } else transparent.setDisable(false);
        return cm;
    }

    public void resetStroke() {
        setStroke(stroke);
        setStyle(style);
    }

    private void listenForDragging() {

        AnchorPane root = MainController.inst.root;

        setOnMousePressed((e1) -> {
            if(Context.nodeMenu != null) {Context.nodeMenu.hide(); Context.nodeMenu = null;}
            if(Context.edgeMenu != null) {Context.edgeMenu.hide(); Context.edgeMenu = null;}
            if(Context.newEdge != null && Context.newEdgeStart != this ) {
                Graph.view.setOnMouseMoved(null);
                Edge edge = Graph.newEdge();
                edge.attach(Context.newEdgeStart, this);
                root.getChildren().remove(Context.newEdge);
                Context.newEdge = null;
                Context.newEdgeStart = null;
            }

            if(Context.shortestPathStart != null && Context.shortestPathStart != this ) {
                Graph.view.setOnMouseMoved(null);
                this.setStroke(Color.RED);
                List<Edge> path = JGraphTService.getShortestPath(Context.shortestPathStart, this);
                for (Edge edge : path) edge.color(Color.RED);
                Context.shortestPathStart.color(Color.RED);
                this.color(Color.RED);
                Context.shortestPathStart = null;
            }

            AtomicBoolean wasSelected = new AtomicBoolean(false);
            AtomicBoolean wasDragged = new AtomicBoolean(false);

            if ( !Selection.nodes.contains(this) ) {
                Selection.selectNode(this);
                wasSelected.set(true);
            }

            SimpleDoubleProperty x0 = new SimpleDoubleProperty(e1.getSceneX()),
                y0 = new SimpleDoubleProperty(e1.getSceneY());

            setOnMouseDragged(e3 -> {
                if (e1.isPrimaryButtonDown()) {
                    if (!wasDragged.get()) {
                        Selection.startDragging();
                        wasDragged.set(true);
                    }
                    double dx = e3.getSceneX() - x0.doubleValue();
                    double dy = e3.getSceneY() - y0.doubleValue();
                    Selection.drag(dx, dy);
                    x0.set(e3.getSceneX());
                    y0.set(e3.getSceneY());
                }
            });

            setOnMouseReleased(e2 -> {
                if (e1.isSecondaryButtonDown()) {
                    Context.nodeMenu = getContextMenu();
                    Context.nodeMenu.show(root, e2.getScreenX(), e2.getScreenY());
                } else {
                    if (wasDragged.get()) {
                        Selection.endDragging();
                    } else if (!wasDragged.get() && !wasSelected.get()) {
                        Selection.deselectNode(this);
                    }
                }
            });

            e1.consume();
        });
    }

    public void select() {
        setStyle("-fx-effect: dropshadow(two-pass-box, #323bff, 3, 1, 0, 0);");
        setStrokeLineCap(StrokeLineCap.ROUND);
    }

    public void deselect() {
        resetStroke();
    }

    public void color(Paint paint) {
        setStroke(paint);
        setStyle("-fx-stroke-width: 4;");
        setStrokeLineCap(StrokeLineCap.ROUND);
    }
}
