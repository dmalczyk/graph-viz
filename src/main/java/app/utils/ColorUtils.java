package app.utils;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ColorUtils {

    public static List<Paint> getColorList() {
        return Arrays.asList(Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.BLUE, Color.VIOLET, Color.MAGENTA);
    }

    public static List<Paint> getRandomColors(int n) {
        List<Paint> colors = getColorList();
        Collections.shuffle(colors);
        return colors.subList(0, n);
    }

    public static Paint getRandomColor() {
        List<Paint> colors = getColorList();
        Collections.shuffle(colors);
        return colors.get(0);
    }
}
