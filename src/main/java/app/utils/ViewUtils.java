package app.utils;

import javafx.stage.Screen;

public class ViewUtils {

    public static double inchesToPixels(double inches) {

        double ppi = Screen.getPrimary().getDpi();
        return ppi * inches;
    }

    public static double pixelsToInches(double pixels) {

        double ppi = Screen.getPrimary().getDpi();
        return pixels / ppi;
    }
}
