package app.actions.changes;

import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ObservableBooleanValue;

import java.util.Stack;

public class ChangeManager {
    private Stack<IChange> undoStack = new Stack<>();
    private Stack<IChange> redoStack = new Stack<>();


    public void undo() {
        if(isUndoAvailable()) {
            IChange command = undoStack.pop();
            command.undo();
            undoAvailable.invalidate();
            if(command.getState() == Change.State.UNDONE) {
                redoStack.push(command);
                redoAvailable.invalidate();
            }
            else {
                this.refresh();
            }
        }
    }

    public void redo() {
        if(isRedoAvailable()) {
            IChange command = redoStack.pop();
            command.redo();
            redoAvailable.invalidate();
            if(command.getState() == Change.State.DONE) {
                undoStack.push(command);
                undoAvailable.invalidate();
            } else {
                this.refresh();
            }
        }
    }

    public void execute (IChange command) {
        command.execute();
        if(command.getState() == Change.State.DONE) {
            undoStack.push(command);
            redoStack.clear();
            undoAvailable.invalidate();
            redoAvailable.invalidate();
        } else {
            this.refresh();
        }
    }

    public void refresh() {
        undoStack.clear(); redoStack.clear();
        undoAvailable.invalidate();
        redoAvailable.invalidate();
    }

    private final BooleanBinding undoAvailable = new BooleanBinding() {
        @Override
        protected boolean computeValue() {
            return !undoStack.empty();
        }
    };

    private final BooleanBinding redoAvailable = new BooleanBinding() {
        @Override
        protected boolean computeValue() {
            return !redoStack.empty();
        }
    };

    public boolean isUndoAvailable() {
        return undoAvailable.get();
    }

    public ObservableBooleanValue undoAvailableProperty() {
        return undoAvailable;
    }

    public boolean isRedoAvailable() {
        return redoAvailable.get();
    }

    public ObservableBooleanValue redoAvailableProperty() {
        return redoAvailable;
    }

}
