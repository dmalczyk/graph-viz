package app.actions.changes;

public  interface IChange {
    enum State{ READY, DONE, UNDONE, ERROR }

    State getState() ;
    void execute() ;
    void undo() ;
    void redo() ;
}
