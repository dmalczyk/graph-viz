package app.actions.changes;

public abstract class Change implements IChange {
    State state = State.READY ;

    public State getState() { return state ; }

    public void execute() {
        assert state == State.READY ;
        try {
            execChange() ;
            state = State.DONE ;
        }
        catch( Throwable e ) { state = State.ERROR; }
    }

    public void undo() {
        assert state == State.DONE ;
        try {
            undoChange();
            state = State.UNDONE;
        }
        catch( Throwable e ) { state = State.ERROR; }
    }

    public void redo() {
        assert state == State.UNDONE ;
        try { execChange() ; state = State.DONE ; }
        catch( Throwable e ) { state = State.ERROR; }
    }

    protected abstract void execChange();
    protected abstract void undoChange();
}