package app.actions.changes.impl;

import app.model.Edge;
import app.actions.changes.Change;
import javafx.scene.paint.Paint;

public class EdgeColorChange extends Change {
    private Edge edge;
    private Paint oldVal;
    private Paint newVal;

    public EdgeColorChange(Edge edge, Paint oldVal, Paint newVal) {
        this.edge = edge;
        this.oldVal = oldVal;
        this.newVal = newVal;
    }

    @Override protected void execChange() {
        edge.stroke = newVal;
        edge.setStroke(newVal);
    }

    @Override protected void undoChange() {
        edge.stroke = oldVal;
        edge.setStroke(oldVal);
    }
}
