package app.actions.changes.impl;

import app.model.Edge;
import app.actions.changes.Change;

public class EdgeStyleChange  extends Change {
    private Edge edge;
    private String oldVal;
    private String newVal;

    public EdgeStyleChange(Edge edge, String oldVal, String newVal) {
        this.edge = edge;
        this.oldVal = oldVal;
        this.newVal = newVal;
    }

    @Override protected void execChange() {
        edge.style = newVal;
        edge.setStyle(newVal);
    }

    @Override protected void undoChange() {
        edge.style = oldVal;
        edge.setStyle(oldVal);
    }
}
