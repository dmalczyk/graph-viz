package app.actions.changes.impl;

import app.model.Node;
import app.actions.changes.Change;
import javafx.scene.paint.Paint;

public class NodeColorChange extends Change {
    private Node node;
    private Paint oldVal;
    private Paint newVal;

    public NodeColorChange(Node node, Paint oldVal, Paint newVal) {
        this.node = node;
        this.oldVal = oldVal;
        this.newVal = newVal;
    }

    @Override protected void execChange() {
        node.stroke = newVal;
        node.setStroke(newVal);
    }

    @Override protected void undoChange() {
        node.stroke = oldVal;
        node.setStroke(oldVal);
    }
}
