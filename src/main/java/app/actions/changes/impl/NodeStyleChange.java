package app.actions.changes.impl;

import app.model.Node;
import app.actions.changes.Change;

public class NodeStyleChange extends Change {
    private Node node;
    private String oldVal;
    private String newVal;

    public NodeStyleChange(Node node, String oldVal, String newVal) {
        this.node = node;
        this.oldVal = oldVal;
        this.newVal = newVal;
    }

    @Override protected void execChange() {
        node.style = newVal;
        node.setStyle(newVal);
    }

    @Override protected void undoChange() {
        node.style = oldVal;
        node.setStyle(oldVal);
    }
}
