package app.actions.changes.impl;

import app.model.Icon;
import app.model.Node;
import app.actions.changes.Change;
import javafx.scene.paint.ImagePattern;

public class IconChange  extends Change {
    private Node node;
    private Icon oldVal;
    private Icon newVal;

    public IconChange(Node node, Icon oldVal, Icon newVal) {
        this.node = node;
        this.oldVal = oldVal;
        this.newVal = newVal;
    }

    @Override protected void execChange() {
        setIcon(newVal);
    }

    @Override protected void undoChange() {
        setIcon(oldVal);
    }

    private void setIcon(Icon icon) {
        node.icon = icon;
        node.setHeight(icon.image.getHeight());
        node.setWidth(icon.image.getWidth());
        node.setFill(new ImagePattern(icon.image));
    }
}
