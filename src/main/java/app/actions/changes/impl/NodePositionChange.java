package app.actions.changes.impl;

import app.model.Node;
import app.actions.changes.Change;

import java.util.LinkedList;
import java.util.Map;

public class NodePositionChange extends Change {
    private Map<Node,LinkedList<Double>> nodes;

    public NodePositionChange(Map<Node,LinkedList<Double>> nodes) { this.nodes = nodes; }

    @Override protected void execChange() {
        for(Node node: nodes.keySet()){
            node.setX(nodes.get(node).get(2));
            node.setY(nodes.get(node).get(3));
        }
    }

    @Override protected void undoChange() {
        for(Node node: nodes.keySet()){
            node.setX(nodes.get(node).get(0));
            node.setY(nodes.get(node).get(1));
        }
    }
}
