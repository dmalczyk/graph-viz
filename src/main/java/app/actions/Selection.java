package app.actions;

import app.controllers.MainController;
import app.model.Edge;
import app.model.Graph;
import app.model.Node;
import app.actions.changes.impl.NodePositionChange;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

import java.util.*;

public class Selection {

    public final static ObservableSet<Node> nodes = FXCollections.observableSet();
    public final static ObservableSet<Edge> edges = FXCollections.observableSet();

    private static class DragInfo {
        List<Rectangle> shapes = new ArrayList();
        Map<Rectangle, Node> mapping = new HashMap<>();
    };

    private static DragInfo dragInfo;

    public static void selectNode(Node node) {
        node.select();
        nodes.add(node);
    }

    public static void deselectNode(Node node) {
        node.deselect();
        nodes.remove(node);
    }

    public static void selectEdge(Edge edge) {
        edge.select();
        edges.add(edge);
    }

    public static void deselectEdge(Edge edge) {
        edge.deselect();
        edges.remove(edge);
    }

    public static void startDragging() {

        dragInfo = new DragInfo();

        nodes.forEach( node -> {

            Rectangle moveShape = new Rectangle();
            Bounds bounds = node.getBoundsInParent();
            System.out.println(bounds);
            Point2D global = node.localToScene(node.getX(), node.getY());
            moveShape.setX(global.getX());
            moveShape.setY(global.getY());
            moveShape.setWidth(node.getWidth());
            moveShape.setHeight(node.getHeight());
            moveShape.setFill(Color.rgb(0, 0, 0, 0.0));
            moveShape.setStroke(Paint.valueOf("red"));
            moveShape.getStrokeDashArray().addAll(2.0, 2.0);
            dragInfo.shapes.add(moveShape);
            dragInfo.mapping.put(moveShape, node);
        });

        dragInfo.shapes.stream().forEach(MainController.inst.root.getChildren()::add);
    }

    public static void drag(double dx, double dy) {

        dragInfo.shapes.stream().forEach(shape -> {
            shape.setX(shape.getX() + dx);
            shape.setY(shape.getY() + dy);
        });
    }

    public static void endDragging() {
        HashMap<Node, LinkedList<Double>> posMap = new HashMap<>();
        dragInfo.shapes.stream().forEach(shape ->{

            Node node = dragInfo.mapping.get(shape);
            Point2D local = node.sceneToLocal(shape.getX(), shape.getY());
            Double[] pos = new Double[]{node.getX(), node.getY(), local.getX(), local.getY()};
            posMap.put(node, new LinkedList<>(Arrays.asList(pos)));

            MainController.inst.root.getChildren().remove(shape);
        });
        NodePositionChange ch = new NodePositionChange(posMap);
        MainController.inst.changeManager.execute(ch);
        clear();
    }

    public static void delete() {

        nodes.stream().forEach(Graph::removeNode);
        clear();
    }

    public static void clear() {

        dragInfo = new DragInfo();
        nodes.stream().forEach(Node::deselect);
        nodes.clear();
        edges.stream().forEach(Edge::deselect);
        edges.clear();
    }
}
