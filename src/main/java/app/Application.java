package app;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Application extends javafx.application.Application {

    public static Stage stage;
    public static Scene scene;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResourceAsStream("/fxml/Main.fxml"));

        stage = primaryStage;
        stage.setTitle("Graph Visualization Tool");
        scene = new Scene(root);
        stage.setScene(scene);
        //stage.setFullScreen(true);
        stage.show();
    }
}
