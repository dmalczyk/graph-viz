package app;

import app.model.Icon;
import app.model.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.shape.Line;

public class Context {

    public static Icon newNode;

    public static Line newEdge;
    public static Node newEdgeStart;

    public static Node shortestPathStart;

    public static ContextMenu nodeMenu;
    public static ContextMenu edgeMenu;
}
